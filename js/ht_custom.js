$(document).ready(function () {
    if (window.innerWidth > 991) {
        $('.department-item:nth-child(2),.department-item:nth-child(3)').wrapAll('<li class="department-wrap"><ul></ul></li>');
    }

    /* change icon menu */
    homeurl = $('#homeurl').val();    
    var allLinks = $('.menu .nav-item .nav-title a').map(function(i,el) { return $(el).attr('href'); }).get();
    var allImg = $('.menu .nav-item img').map(function(i,el) { return $(el).attr('src'); }).get();
    var allImgCate = $('.department-list img').map(function(i,el) { return $(el).attr('src'); }).get();
    for( var i=0; i<allLinks.length; i++){        
        if(allLinks[i] === '#'){
            for( var j=0; j<allImg.length; j++){                
                allImg[i] = homeurl + '/assets/images/icons/ergox-icon-department.png';
                $('.menu .nav-item img').map(function(i,el) { 
                    $(el).attr('src', allImg[i]);
                });
            }
        }
        else{
            temp = allLinks[i].slice( homeurl.length + 1 );
            temp = temp.slice(0,temp.length - 1);
            allImg[i] = homeurl + '/assets/images/icons/' + temp + '.png';
            $('.menu .nav-item img').map(function(i,el) { 
                $(el).attr('src', allImg[i]);
            });
            $('.menu .nav-item .nav-title img').map(function(i,el) { 
                $(el).attr('src', allImg[i]);
            });
            $('.department-list .des img').map(function(i,el) { 
                $(el).attr('src', allImg[i+1]);
            });
        }
    }

    $('.best-sellers .owl-carousel').owlCarousel({
        loop: false,
        responsiveClass: true,
        responsive: {
            0: {
                items: 2,
                margin: 10
            },
            768: {
                items: 3,
                margin: 25
            },
            992: {
                items: 4,
                margin: 10
            },
            1170: {
                items: 5,
                margin: 20
            },
            1920: {
                items: 5,
                margin: 70
            }
        }
    });

    $('.wrapper-testimonials .owl-carousel').owlCarousel({
        loop: false,
        margin: 100,
        responsiveClass: true,
        nav: true,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            992: {
                items: 1,
                margin: 0
            }
        }
    });

    $('.menu-mobile').on('click', function () {
        $('.menu').toggleClass('active');
        $('.menu-mobile').toggleClass('active');
    });

    /* search */
    if (window.innerWidth < 768) {
        $('.search-container .btn-mobile').on('click', function (event) {
            $(this).hide();
            $('.wrapper-logo').toggleClass('hide');
            $('.search-container').toggleClass('active');
            $('.header-search .ajax_search').toggleClass('active');
            $('.header-search').toggleClass('active');
            $('.login-cart').toggleClass('hide');
            $('.search-container .input-group-append').toggleClass('active');
        });
        $('.search-container .d-none').on('click', function (event) {
            // event.preventDefault();
            $('.search-container .btn-mobile').show();
            $('.wrapper-logo').removeClass('hide');
            $('.search-container').removeClass('active');
            $('.header-search .ajax_search').removeClass('active');
            $('.header-search').removeClass('active');
            $('.login-cart').removeClass('hide');
            $('.search-container .input-group-append').removeClass('active');
        });
    }

    /* category page */
    $('.trustmaker-category .owl-carousel').owlCarousel({
        loop: false,
        responsiveClass: true,
        responsive: {
            0: {
                items: 2,
                autoplay: true,
                autoplayTimeout: 3000,
                autoplayHoverPause: true,
                dots: false
            },
            768: {
                items: 4
            }
        }
    });

    function checkShowAll(className) {
        if ($('#left-sidebar .' + className + ' > ul > li').length > 7) {
            $('#left-sidebar .' + className + ' p.show-all').show();
        }
    }

    checkShowAll('filter-by-category');
    checkShowAll('filter-by-brand');
    $('.filter-by-name').each(function () {
        var a = $(this).val();
        checkShowAll(a);
    });

    $('p.show-all').click(function () {
        $(this).prev().children().show('slow');
        $(this).hide();
        $(this).next().show();
    });
    $('p.show-less').click(function () {
        $(this).prev().prev().children('li:nth-child(7)').nextAll().hide('slow');
        $(this).hide();
        $(this).prev().show();
    });

    $('.show-filter').click(function () {
        $(this).hide();
        $('#left-sidebar').show();
        $('#left-sidebar').attr('style', 'display:block');
        $('.list-product-content').addClass('show');
        if($(window).width() > 767){
            $('.panel-heading-thumbnails .sort_container').css('display','none');
        } else{
            $('.panel-heading-thumbnails .button-filter').css('display','none');
            $('.panel-heading-thumbnails .sort_container').css('marginBottom', '15px');
        }
        
    })
    $('.hide-filter').click(function () {
        $('.show-filter').show();
        $('#left-sidebar').hide();
        $('.list-product-content').removeClass('show');
    })

    /* detail page */
    $('.thumbnail-images.owl-carousel').owlCarousel({
        loop: false,
        nav: true,
        dots: false,
        responsiveClass: true,
        margin: 10,
        responsive: {
            0: {
                items: 2
            },
            768: {
                items: 3
            },
            992: {
                items: 4
            },
            1200: {
                items: 6
            }
        }
    });

    $('.also-like .owl-carousel').owlCarousel({
        loop: false,
        responsiveClass: true,
        responsive: {
            0: {
                items: 2,
                margin: 10
            },
            768: {
                items: 3,
                margin: 30
            },
            992: {
                items: 4,
                margin: 20
            },
            1920: {
                items: 4,
                margin: 30
            }
        }
    });

    var lastScrollTop = 0;
    $(window).scroll(function (event) {
        var st = $(this).scrollTop();
        if (st > lastScrollTop) {
            $('.top-left').animate({ left: '0%' }, 50);
            $('.bottom-right').animate({ right: '0%' }, 50);
        } else {
            $('.top-left').animate({ left: '-5%' }, 50);
            $('.bottom-right').animate({ right: '+5%' }, 50);
        }
        lastScrollTop = st;
    });

    $("#department-jump").click(function () {
        $('html,body').animate({
            scrollTop: $(".department-jump").offset().top
        },
            'slow');
    });
    var currentUrl = window.location.href;
    currentUrl = currentUrl.slice( homeurl.length + 1 );
    currentUrl = currentUrl.slice(0,currentUrl.length - 1);
    $('ul.link-subpages li').each(function(){
        subHref = homeurl+$(this).children().attr("href");
        subHref = subHref.slice( homeurl.length + 1 );
        subHref = subHref.slice(0,subHref.length - 1);
        if( subHref === currentUrl){
            $(this).addClass('active');
        }
    });
});